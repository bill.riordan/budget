<h1>:fire: Setup :fire:</h1>
<ol>
<li>pull repository</li>
<li>composer install</li>
<li>download a .csv file from Chase</li>
<li>run <code>php artisan read:csv 'path/to/spreadsheet'</code></li>
<li>run <code>php artisan serve</code></li>
<li>navigate to that page</li>
</ol>

<hr>
The page will be split in 2: left is income, right is expenses.
There will be a vertical list of transactions. Transactions are 
broken down into a list of tags. To associate a tag with a 
keyword/type, just click on a tag.

For instance; If you press on a tag labelled 'wings' and enter 
'food' into the alert box, then all transactions with a tag labelled 
'wings' will be associated as transactions for food.

Two caveats: this association doesn't take effect until you run
the `php artisan read:csv {filepath}` command again, and type
associations are first come first serve. This means that if a 
transaction has both 'wings' and 'beer' as tags, then the type
will be 'food' if 'wings' appears first in the tags list.

:money_with_wings:
@extends('layouts.default')
<script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.4.0/Chart.min.js"></script>
<script
  src="https://code.jquery.com/jquery-3.3.1.min.js"
  integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8="
  crossorigin="anonymous"></script>

<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">

<div id="income" class="col-md-6">
<canvas id="incomeChart" height="500px"></canvas>
<script>
    var ctx = document.getElementById("incomeChart").getContext("2d");
    var chart = new Chart(ctx, {
        // The type of chart we want to create
        type: "pie",
    
        // The data for our dataset
        data: {
            labels: @json($incomeKeys),
            datasets: [
                {
                    label: "data",
                    backgroundColor: @json($incomeColors),
                    borderColor: "rgb(0, 0, 0)",
                    data: @json($incomeData)
                }
            ]
        },
    
        // Configuration options go here
        options: {
            responsive:false,
            maintainAspectRatio: false,
            cutoutPercentage: 70
        }
    });
</script>
	@foreach($income as $payment)
			<div class="panel panel-default">
                <div class="panel-heading">
                    <span class="badge badge-success">{{$payment->amount}}</span>
                </div>
                <div class="panel-body">
				@foreach($payment->tags as $tag)
					<span id="{{$tag->id}}" class="label label-info">{{$tag->name}}</span>
				@endforeach
                </div>
                <div class="panel-footer">
                    @if(!empty($payment->type))
                    {{ $payment->type->descriptor }}
                    @endif
                </div>
			</div>
	@endforeach
</div>
<div id="expenses" class="col-md-6">
<canvas id="expensesChart" height="500px"></canvas>
<script>
    var ctx = document.getElementById("expensesChart").getContext("2d");
    var chart = new Chart(ctx, {
        // The type of chart we want to create
        type: "pie",
    
        // The data for our dataset
        data: {
            labels: @json($expensesKeys),
            datasets: [
                {
                    label: "data",
                    backgroundColor: @json($expensesColors),
                    borderColor: "rgb(0, 0, 0)",
                    data: @json($expensesData)
                }
            ]
        },
    
        // Configuration options go here
        options: {
            responsive:false,
            maintainAspectRatio: false,
            cutoutPercentage: 70
        }
    });
</script>
	@foreach($expenses as $payment)
            <div class="panel panel-default">
                <div class="panel-heading">
                    <span class="badge badge-warning">{{$payment->amount}}</span>
                </div>
                <div class="panel-body">
                @foreach($payment->tags as $tag)
                    <span id="{{$tag->id}}" class="label label-warning">{{$tag->name}}</span>
                @endforeach
                </div>
                <div class="panel-footer">
                    @if(!empty($payment->type))
                    {{ $payment->type->descriptor }}
                    @endif
                </div>
            </div>
    @endforeach
</div>

<script>
    var elements = document.getElementsByClassName("label");

    var myFunction = function() {
        var tagId = this.getAttribute("id");
        popUpPrompt(tagId);
    };

    for (var i = 0; i < elements.length; i++) {
        elements[i].addEventListener('click', myFunction, false);
    }

    function popUpPrompt(tagId) {
        var txt;
        var word = prompt("what type of tag is this?", "food");
        if (word == null || word == "") {
            //cancelled prompt
        } else {
            $.ajax({
                type: "POST",
                url: '/keyword/add',
                data: { tag_id: tagId, word: word }, 
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                success: function( msg ) {
                    //alert( msg );
                }
            });
        }
    }
</script>
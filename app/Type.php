<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Type extends Model
{
    protected $fillable = [
        'descriptor',
    ];

    protected $hidden = [
        'id', 'created_at', 'updated_at',
    ];

    public function tags()
    {
        return $this->hasMany('App\Tag');
    }

    public function keywords()
    {
        return $this->hasMany('App\Keyword');
    }

    public function transactions()
    {
        return $this->hasMany('App\Transaction');
    }
}

<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Collection;
use Carbon\Carbon;
use App\SpreadSheet;
use App\Transaction;
use App\Tag;
use App\Type;
use App\Keyword;

class ReadCSV extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'read:csv {filepath}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'parse csv data';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $oldTransactions = Transaction::all();
        foreach($oldTransactions as $transaction) {
            $transaction->delete();
        }
        $oldTags = Tag::all();
        foreach($oldTags as $tag) {
            $tag->delete();
        }
        // read excel sheet
        // 'public\csv\Chase2912_Activity_20180825.CSV'
        \Excel::load($this->argument('filepath'))->each(function (Collection $csvLine) {
            //do stuff
            $transaction = new Transaction();
            $transaction->amount = (int) ($csvLine->get('amount') * 100);
            $transaction->transaction_date = new Carbon($csvLine->get('posting_date'));
            $transaction->save();

            $line = preg_replace("/[\s]/", " ", $csvLine->get('description'));
            foreach (preg_split("/[^A-Za-z]+/", $line) as $word) {
                if($word == '')
                {
                    continue;
                }
                $tag = new Tag();
                $tag->name = strtoLower($word);
                $tag->transaction_id = $transaction->id;
                $tag->save();
            }
        });

        // link keywords to transactions
        $transactions = Transaction::all();
        foreach ($transactions as $transaction) {
            foreach ($transaction->tags as $tag) {
                $keyword = Keyword::where('word', $tag->name)->first();
                if ($keyword) {
                    $transaction->type_id = $keyword->type_id;
                    $transaction->save();
                    break;
                }
            }
        }
    }
}

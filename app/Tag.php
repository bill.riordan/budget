<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Tag extends Model
{
    protected $fillable = [
        'transaction_id', 'name', 'type_id',
    ];

    protected $hidden = [
        'id', 'created_at', 'updated_at',
    ];

    public function transaction()
    {
        return $this->belongsTo('App\Transaction');
    }

    public function type()
    {
        return $this->belongsTo('App\Type');
    }
}

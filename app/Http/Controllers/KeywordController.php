<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\SpreadSheet;
use App\Transaction;
use App\Tag;
use App\Type;
use App\Keyword;

class KeywordController extends Controller
{
    //

    public function store(Request $request)
    {
        $keyword = new Keyword();

        $tag = Tag::find($request->input('tag_id'));
        $type = Type::where('descriptor', $request->input('word'))->first();

        if(!$type) {
        	$type = new Type();
        	$type->descriptor = $request->input('word');
        	$type->save();
        }

        $keyword->type_id = $type->id;
        $keyword->word = $tag->name;
        $keyword->save();

        return response()->json(array('msg'=> $keyword->id), 200);
    }
}

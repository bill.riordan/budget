<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Collection;
use Carbon\Carbon;
use App\SpreadSheet;
use App\Transaction;
use App\Tag;
use App\Type;

class HomeController extends Controller
{
    public function index()
    {
    	$expenses = Transaction::getExpenses();
    	$income = Transaction::getIncome();

    	$graphArray = [];
    	$types = Type::all();
    	foreach ($types as $type) {
    		$graphArray[$type->descriptor] = 0;
		}


		// get expense data
		$expensesData = [];
		$collation = Transaction::getExpensesForGraph($graphArray);
		asort($collation);
		foreach($collation as $datum) {
			$expensesData[] = $datum;
		}
		$expensesKeys = array_keys($collation);

		// get income data
		$incomeData = [];
		$collation = Transaction::getIncomeForGraph($graphArray);
		asort($collation);
		foreach($collation as $datum) {
			$incomeData[] = $datum;
		}
		$incomeKeys = array_keys($collation);

		$incomeColors = SpreadSheet::getWeightedColors(0.5,0.5,1,count($expensesKeys));
		$expensesColors = SpreadSheet::getWeightedColors(1,0.5,0.5,count($expensesKeys));

    	return view('index', [
    		'expenses' 			=> $expenses, 
    		'income' 			=> $income, 

    		'expensesData' 		=> $expensesData, 
    		'expensesColors'	=> $expensesColors,
    		'expensesKeys' 		=> $expensesKeys,

    		'incomeData' 		=> $incomeData, 
    		'incomeColors'		=> $incomeColors,
    		'incomeKeys' 		=> $incomeKeys
    	]);
    }
}

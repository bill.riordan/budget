<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Keyword extends Model
{
    protected $fillable = [
        'word', 'type_id'
    ];

    protected $hidden = [
        'id', 'created_at', 'updated_at',
    ];

    public function type()
    {
        return $this->belongsTo('App\Type');
    }
}

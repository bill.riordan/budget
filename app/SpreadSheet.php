<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SpreadSheet extends Model
{
    protected $fillable = [
        'name',
    ];

    protected $hidden = [
        'id', 'created_at', 'updated_at',
    ];

    public function transactions()
    {
        return $this->hasMany('App\Transaction');
    }

    public static function getWeightedColors($red, $green, $blue, $count)
    {
        $colors = [];
        for($i = 0; $i < $count; $i++) {
            // I think random is more fun
            $color = 'rgb(';
            $color .= floor((mt_rand() / mt_getrandmax()) * (255 /** $red*/)) . ',';
            $color .= floor((mt_rand() / mt_getrandmax()) * (255 /** $green*/)) . ',';
            $color .= floor((mt_rand() / mt_getrandmax()) * (255 /** $blue*/)) . ')';
            $colors[] = $color;
        }
        return $colors;
    }
}

<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Transaction extends Model
{
    protected $fillable = [
        'amount', 'transaction_date', 'type_id'
    ];

    protected $hidden = [
        'id', 'created_at', 'updated_at',
    ];

    public function tags()
    {
        return $this->hasMany('App\Tag');
    }

    public function type()
    {
        return $this->belongsTo('App\Type');
    }

    public function spreadSheet()
    {
    	return $this->belongsTo('App\SpreadSheet');
    }

    public static function getExpenses()
    {
        return Transaction::where('amount', '<', 0)->get();
    }

    public static function getIncome()
    {
        return Transaction::where('amount', '>', 0)->get();
    }

    public static function getExpensesForGraph($types)
    {
        $expenses = Transaction::where('amount', '<', 0)->where('type_id', '!=', NULL)->get();
        foreach($expenses as $expense) {
            $types[$expense->type->descriptor] += abs($expense->amount);
        }
        foreach($types as &$type) {
            $type = number_format(($type/100), 2, '.', '');
        }
        return $types;
    }

    public static function getIncomeForGraph($types)
    {
        $expenses = Transaction::where('amount', '>', 0)->where('type_id', '!=', NULL)->get();
        foreach($expenses as $expense) {
            $types[$expense->type->descriptor] += abs($expense->amount);
        }
        foreach($types as &$type) {
            $type = number_format(($type/100), 2, '.', '');
        }
        return $types;
    }
}
